//
//  WeatherItem.swift
//  weather-mvc
//
//  Created by toor on 8/26/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation

struct WeatherItem {
    var timeOfDay: String
    var urlWeatherIcon: String 
    var temp: String
    var humidity: String
    var pressure: String
    var wind: String
    var imgData: Data?
}

struct WeatherDay {
    var date: String
    var dayOfWeek: String
    var sunrise: String
    var sunset: String
    var morning, day, evenig, night: WeatherItem
}
