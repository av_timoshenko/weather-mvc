//
//  WeatherDayManager.swift
//  weather-mvc
//
//  Created by toor on 8/26/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation

class WeatherDayManager {
    var count = 0
    var weatherDays: [Int:WeatherDay] = [:]
    var delegate : DelegatWeatherDay? = nil
    var curentDay: Date = Calendar.current.startOfDay(for: Date())
    var SiteCityCode : String? = "26850" {
        willSet (newValue) {
            if newValue != self.SiteCityCode { //clear all data
                clearData()
            }
        }
    }
    
    
    static var mainSemaphore : DispatchSemaphore = DispatchSemaphore(value: 1)
    
    subscript(index: Int) -> WeatherDay {
        get {
            return weatherDays[index]!
        }
    }
    
    func clearData()  {
        weatherDays = [:]
        count = 0
        curentDay =  Calendar.current.startOfDay(for: Date())
        delegate?.didDownloadWeatherDay(batchSize: 0, batchStartIndex: 0)
    }
    
    func next(batchSize: Int) {
        WeatherDayManager.mainSemaphore.wait()
        let semaphore = DispatchSemaphore(value: 1)
        var batchCount = 0
        let countDaysAtStart = self.weatherDays.count
        
        for batchIndex in 1...batchSize {
            DispatchQueue.global().async {
                //first cuncurent section
                semaphore.wait()
                let insertIndex = countDaysAtStart + batchIndex - 1
                self.curentDay = Calendar.current.date(byAdding: .day, value: -1,to: self.curentDay)!
                semaphore.signal()
                
                let data = WGet.download(urlString: "https://pogoda.tut.by/archive.html?date_from=\(self.curentDay.timeIntervalSince1970)&date_to=\(self.curentDay.timeIntervalSince1970)&city=\(self.SiteCityCode!)")
                
                let wd = WeatherParser.parse(data)
               
                //second cuncurent section
                semaphore.wait()
                if let weatherData = wd {
                    self.weatherDays[insertIndex] = weatherData
                    self.count  =  self.weatherDays.count
                }
                batchCount += 1
                
                if batchCount == batchSize {
                    self.delegate?.didDownloadWeatherDay(batchSize: batchSize, batchStartIndex: self.weatherDays.count - batchSize)
                    WeatherDayManager.mainSemaphore.signal()
                }
                semaphore.signal()
            }
        }
    }
    
    
}


protocol DelegatWeatherDay {
    func didDownloadWeatherDay(batchSize: Int, batchStartIndex: Int)
}
