//
//  MainTabBarController.swift
//  weather-mvc
//
//  Created by toor on 16.08.2019.
//  Copyright © 2019 toor. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if Defaults[.addr] == nil {
            selectedIndex = 0
        } else {
            selectedIndex = 1
        }
        
    }
    
}
