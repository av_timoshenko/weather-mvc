//
//  WeatherTableViewController.swift
//  weather-mvc
//
//  Created by toor on 16.08.2019.
//  Copyright © 2019 toor. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class WeatherTableViewController: UITableViewController {

    let wdManager = (UIApplication.shared.delegate as! AppDelegate).wdManager
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "UIWeatherCell", bundle: nil), forCellReuseIdentifier: "WeatherCell")
        wdManager.delegate = self
        if let ccode = Defaults[.cityCode] {
            wdManager.SiteCityCode = ccode
        }
        wdManager.next(batchSize: 1)
        wdManager.next(batchSize: 1)
        wdManager.next(batchSize: 1)
        wdManager.next(batchSize: 1)
        wdManager.next(batchSize: 1)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return wdManager.count + 1
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! UIWeatherCell
        
        if (indexPath.row == wdManager.count) {
            wdManager.next(batchSize: 5)
        } else {
            cell.dayOfWeekLabel.text = wdManager[indexPath.row].dayOfWeek
            cell.dateLabel.text = wdManager[indexPath.row].date
            cell.sunsetLabel.text = wdManager[indexPath.row].sunset
            cell.sunriseLabel.text = wdManager[indexPath.row].sunrise
            
            cell.imageMorning.image = UIImage(data: wdManager[indexPath.row].morning.imgData!)
            cell.timeOfDayLabelMorning.text = wdManager[indexPath.row].morning.timeOfDay
            cell.tempLabelMorning.text = wdManager[indexPath.row].morning.temp
            
            cell.imageDay.image = UIImage(data: wdManager[indexPath.row].day.imgData!)
            cell.timeOfDayLabelDay.text = wdManager[indexPath.row].day.timeOfDay
            cell.tempLabelDay.text = wdManager[indexPath.row].day.temp
            
            cell.imageEvening.image = UIImage(data: wdManager[indexPath.row].evenig.imgData!)
            cell.timeOfDayLabelEvening.text = wdManager[indexPath.row].evenig.timeOfDay
            cell.tempLabelEvening.text = wdManager[indexPath.row].evenig.temp
            
            cell.imageNight.image = UIImage(data: wdManager[indexPath.row].night.imgData!)
            cell.timeOfDayLabelNight.text = wdManager[indexPath.row].night.timeOfDay
            cell.tempLabelNight.text = wdManager[indexPath.row].night.temp
            
            
        }
        
        return cell
    }
 
}

    
extension WeatherTableViewController : DelegatWeatherDay {
    func didDownloadWeatherDay(batchSize: Int, batchStartIndex: Int) {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
    }
    
    
}
