//
//  ViewController.swift
//  weather-mvc
//
//  Created by toor on 15.08.2019.
//  Copyright © 2019 toor. All rights reserved.
//

import UIKit
import YandexMapKit
import SwiftyJSON
import SwiftyUserDefaults

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var lblLatitude: UILabel!
    @IBOutlet weak var lblLongitude: UILabel!
    @IBOutlet weak var lblAdress: UILabel!
    
    @IBOutlet weak var mapView: YMKMapView!
    
    var locationManager: YMKLocationManager!
    var curentPositin = YMKPoint()
    var addr = ""
    
    let wdManager = (UIApplication.shared.delegate as! AppDelegate).wdManager
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = YMKMapKit.sharedInstance()!.createLocationManager()
        mapView.mapWindow.map.addTapListener(with: self)
        restoreFromDefaults()
        moveMapToCurrentPosition()
        setMapPosition()
        
    }
    
    @IBAction func btnPlaceTap(_ sender: Any) {
        locationManager.requestSingleUpdate(withLocationListener: self)
    }
    
    
    @IBAction func btsSave(_ sender: Any) {
        saveToDefaults()
        let tabBarController = self.parent as! UITabBarController
        tabBarController.selectedIndex = 1
    }
    
    func moveMapToCurrentPosition(){
        DispatchQueue.main.async {
            self.mapView.mapWindow.map.move(
                with: YMKCameraPosition(target: self.curentPositin, zoom: 18, azimuth: 0, tilt: 0),
                animationType: YMKAnimation(type: YMKAnimationType.linear, duration: 2),
                cameraCallback: nil)
            
        }
    }
    
    func restoreFromDefaults() {
        curentPositin = YMKPoint(latitude: Defaults[.latitude] ?? 53.894055, longitude: Defaults[.longitude] ?? 27.419439)
        addr = Defaults[.addr] ?? "Беларусь, Минск, улица Скрипникова, 29"
    }
    
    func saveToDefaults() {
        Defaults[.latitude] = curentPositin.latitude
        Defaults[.longitude] = curentPositin.longitude
        Defaults[.addr] = addr
        Defaults[.cityCode] = Cities.getCityCode(addr)
        wdManager.SiteCityCode = Defaults[.cityCode]
    }

    func setMapPosition(){
        DispatchQueue.main.async {
            self.mapView.mapWindow.map.mapObjects.clear()
            self.mapView.mapWindow.map.mapObjects.addPlacemark(with: self.curentPositin)
            self.lblAdress.text =  "Адрес: \(self.addr)"
            self.lblLatitude.text = "Широта: \(String(format: "%.4f",self.curentPositin.latitude))"
            self.lblLongitude.text = "Долгота: \(String(format: "%.4f",self.curentPositin.longitude))"
        }
    }
    
    func getAddres(latitude: Double, longitude: Double) -> String {
        var retStr = ""
        
        let urlString = "https://geocode-maps.yandex.ru/1.x/?apikey=9e02d696-b1ba-4198-9afc-68db4768fc18&format=json&geocode=\(longitude),\(latitude)"
        
        let responseData = WGet.download(urlString: urlString)
       
        if let rd = responseData {
            do {
                let json = try JSON(data: rd)
                retStr = json["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["text"].string ?? ""
                
            } catch {
                retStr = ""
            }
        }
        
        return retStr
    }
    
    
}

extension SettingsViewController : YMKLocationDelegate {
    func onLocationUpdated(with location: YMKLocation) {
        DispatchQueue.global(qos: .userInitiated).async {
            self.curentPositin = location.position
            self.addr = self.getAddres(latitude: self.curentPositin.latitude, longitude: self.curentPositin.longitude)
            self.moveMapToCurrentPosition()
            self.setMapPosition();
            
        }
    }
    
    func onLocationStatusUpdated(with status: YMKLocationStatus) {
        
    }
    
    
}

extension SettingsViewController : YMKLayersGeoObjectTapListener {
    func onObjectTap(with event: YMKGeoObjectTapEvent) -> Bool {
        self.curentPositin = event.geoObject.geometry[0].point!
        DispatchQueue.global(qos: .userInitiated).async {
            self.addr = self.getAddres(latitude: self.curentPositin.latitude, longitude: self.curentPositin.longitude)
            self.setMapPosition();
        }
        
        return true
    }
}

