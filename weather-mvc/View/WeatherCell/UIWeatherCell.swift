//
//  UIWeatherCell.swift
//  weather-mvc
//
//  Created by toor on 8/28/19.
//  Copyright © 2019 toor. All rights reserved.
//

import UIKit

class UIWeatherCell: UITableViewCell {

    @IBOutlet weak var dayOfWeekLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var sunriseLabel: UILabel!
    
    @IBOutlet weak var sunsetLabel: UILabel!
    
  
    //Morning
    @IBOutlet weak var timeOfDayLabelMorning: UILabel!
    
    @IBOutlet weak var imageMorning: UIImageView!
    
    @IBOutlet weak var tempLabelMorning: UILabel!
    
    //Day
    @IBOutlet weak var timeOfDayLabelDay: UILabel!
    
    @IBOutlet weak var imageDay: UIImageView!
    
    @IBOutlet weak var tempLabelDay: UILabel!
    
    //Evening
    @IBOutlet weak var timeOfDayLabelEvening: UILabel!
    
    @IBOutlet weak var imageEvening: UIImageView!
    
    @IBOutlet weak var tempLabelEvening: UILabel!
    
    //Night
    @IBOutlet weak var timeOfDayLabelNight: UILabel!
    
    @IBOutlet weak var imageNight: UIImageView!
    
    @IBOutlet weak var tempLabelNight: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
