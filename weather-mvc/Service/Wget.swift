//
//  WGet.swift
//  weather-mvc
//
//  Created by toor on 8/26/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation

class Cache {
    var cache: [String:Data] = [:]
    subscript(name: String) -> Data? {
        get {
            return cache[name]
        }
        set (newValue) {
            cache[name] = newValue
        }
    }
}

class WGet {
    static var cache = Cache()
    
    static func download(urlString: String, canCached: Bool = false) -> Data? {
        var retData: Data?
        if canCached {
            let val = cache[urlString]
            if val != nil {
                return val
            }
        }
        
        guard let url = URL(string: urlString) else {
            return nil
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let semaphore = DispatchSemaphore(value: 0)
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                retData = nil
                return
            }
            // make sure we got data
            guard let responseData = data else {
                retData = nil
                return
            }
            retData = responseData
            semaphore.signal()
        }
        task.resume()
        semaphore.wait()
        
        if canCached && retData != nil {
            cache[urlString] = retData!
        }
        
        return retData
    }
}
